﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System.Diagnostics;
using Emgu.CV.Util;
using System.IO;

namespace GenderPCA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Image<Bgr, byte> imgInit = new Image<Bgr, byte>("FE002.jpg");
            Image<Bgr, byte> imgDraw = imgInit.Clone();

            // Deklarasi Class ClassDetecion
            ClassDetection CD = new ClassDetection(imgInit);
            
            /* Memberikan value pada variable global FileCascade
             * Ketepatan File Cascade(HAARCASCADE)
             * index 0 = face
             * index 1 = eyes
             * index 2 = nose
             * index 3 = mouth
            */

            CD.FileCascade = new String[4] { "haarcascade_frontalface_default.xml", "haarcascade_eye.xml", "haarcascade_mcs_nose.xml", "haarcascade_mcs_mouth.xml" };            

            /* Return value dari pemanggilan function dari ClassDetection
             * DetectFace()     = deteksi wajah 
             * DetectFeature(1) = deteksi mata
             * DetectFeature(2) = deteksi  Hidung
             * DetectFeature(3) = deteksi Mulut 
            */

            List<Rectangle> roiFace  = CD.DetectFace();
            List<Rectangle> roiEye   = CD.DetectFeature(1);
            List<Rectangle> roiNose  = CD.DetectFeature(2);
            List<Rectangle> roiMouth = CD.DetectFeature(3);

            /* Pemecahan ROI untuk draw rectangle pada image */
            foreach (Rectangle fc in roiFace)
            {
                imgDraw.Draw(fc, new Bgr(Color.Red), 1);
            }

            foreach (Rectangle fe in roiEye)
            {
                imgDraw.Draw(fe, new Bgr(Color.Green), 1);
            }

            foreach (Rectangle fn in roiNose)
            {
                imgDraw.Draw(fn, new Bgr(Color.Blue), 1);
            }

            foreach (Rectangle fm in roiMouth)
            {
                imgDraw.Draw(fm, new Bgr(Color.Yellow), 1);
            }

            imageBox1.Image = imgDraw;
        }
    }
}
